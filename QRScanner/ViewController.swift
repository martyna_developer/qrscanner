//
//  ViewController.swift
//  QRScanner
//
//  Created by Martyna Wiśnik on 16.08.2018.
//  Copyright © 2018 Martyna Wiśnik. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class ViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var scannerPlace: UIImageView!
    
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        captureSession = AVCaptureSession()
        titleLabelSettings()
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }
        let metadataOutput = AVCaptureMetadataOutput()
        
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
        } else {
            failed()
            return
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = scannerPlace.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        scannerPlace.layer.addSublayer(previewLayer)
        
    }

    func failed() {
        let ac = UIAlertController(title: "Scanning does not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera." , preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        
        captureSession.stopRunning()
        if let metadataObject = metadataObjects.first {
            let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject
            let stringValue = readableObject?.stringValue
            let url = URL(string: stringValue!)
            print(url as Any)
            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
         
            
        } else {
            return
        }
        dismiss(animated: true)
    }
    
    func titleLabelSettings() {
        titleLabel.text = "QR code scanner"
        titleLabel.textAlignment = NSTextAlignment.center
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.numberOfLines = 1
        titleLabel.autoresizingMask = UIViewAutoresizing.flexibleRightMargin
        titleLabel.minimumScaleFactor = 10 / UIFont.labelFontSize
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
    }
    
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }


}

